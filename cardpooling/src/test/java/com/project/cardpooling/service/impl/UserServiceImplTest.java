package com.project.cardpooling.service.impl;

import com.project.cardpooling.dto.UserDto;
import com.project.cardpooling.factory.FactoryUserDataTest;
import com.project.cardpooling.mapper.UserMapper;
import com.project.cardpooling.model.User;
import com.project.cardpooling.repository.IUserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    IUserRepository userRepository;

    @Mock
    UserMapper userMapper;

    @Test
    void SaveUser() {
        //Give
        UserDto userDto = new FactoryUserDataTest().getUserDto();
        User expecteUser = new FactoryUserDataTest().getUser();

        //When
        Mockito.when(userMapper.getEntityFromDto(Mockito.any())).thenReturn(expecteUser);
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(expecteUser);

        userService.saveUser(userDto);

        //Then
        assertEquals(expecteUser.getName(), "David");
        //Mockito.verify(userRepository.save(Mockito.any()));
    }

    @Test
    void returnNullSaveUser() {
        //Give
        UserDto userDto = new FactoryUserDataTest().getUserDto();
        User expecteUser = null;

        //When
        Mockito.when(userMapper.getEntityFromDto(Mockito.any())).thenReturn(expecteUser);
        Mockito.when(userRepository.save(Mockito.any())).thenReturn(expecteUser);

        userService.saveUser(userDto);

        //Then
        assertEquals(null, expecteUser);
        //Mockito.verify(userRepository.save(Mockito.any()));
    }

    @Test
    void findUserByEmail() {

        //Give
        UserDto userDto = new FactoryUserDataTest().getUserDto();
        List<User> expecteUser = new FactoryUserDataTest().getUserList();

        //
        Mockito.when(userRepository.findUserByEmail(Mockito.anyString())).thenReturn(expecteUser);

        //
        Assertions.assertNotNull(expecteUser);
        Assertions.assertEquals(userDto.getEmail(), "user@gmail.com");
    }

    @Test
    void returnNullNoFindUserByEmail() {

        //Give
        List<User> expecteUser = null;

        //When
        Mockito.when(userRepository.findUserByEmail(Mockito.anyString())).thenReturn(expecteUser);

        //
        Assertions.assertEquals(null, expecteUser);
    }
}