package com.project.cardpooling.factory;

import com.project.cardpooling.dto.UserDto;
import com.project.cardpooling.model.User;

import java.util.List;

public class FactoryUserDataTest {

    public  static User getUser(){

        User user = new User();
        user.setId(1);
        user.setName("David");
        user.setLastname("Number1");
        user.setAddress("3k");
        user.setTelephone("123456");
        user.setEmail("user@gmail.com");
        user.setPassword("w3Unpocode@*");

        return user;
    }

    public  static List<User> getUserList(){

        User user = getUser();
        List<User> userList = List.of(user);
        return userList;
    }

    public  static UserDto getUserDto(){

        UserDto userDto = new UserDto();
        userDto.setId(1);
        userDto.setName("David");
        userDto.setLastname("Number1");
        userDto.setAddress("3k");
        userDto.setTelephone("123456");
        userDto.setEmail("user@gmail.com");
        userDto.setPassword("w3Unpocode@*");

        return userDto;
    }
}
