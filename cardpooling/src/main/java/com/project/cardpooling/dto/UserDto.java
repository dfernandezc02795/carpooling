package com.project.cardpooling.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@Getter
@Setter
public class UserDto {

    private long id;

    @NotBlank(message = "The name cannot be empty")
    private String name;

    @NotBlank(message = "The lastname cannot be empty")
    private String lastname;

    @NotBlank(message = "The telephone cannot be empty")
    private String telephone;

    @NotBlank(message = "The address cannot be empty")
    private String address;

    @NotEmpty(message = "The email cannot be empty")
    @Email(message = "The email format is incorrect")
    private String email;

    @Size(min = 8, max = 15, message = "The longitud minimum is 8 and the logitud maximum" +
            " is 15")
    @NotEmpty(message = "The password cannot be empty")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[*_-])[A-Za-z\\d*_-]{8,15}$",
            message = "the password must contain uppercase letters, lowercase letters, numbers and one character")
    private String password;
}
