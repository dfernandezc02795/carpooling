package com.project.cardpooling.service;

import com.project.cardpooling.dto.UserDto;

import java.util.List;

public interface IUserService {
    UserDto saveUser(UserDto user);
    List<UserDto> findUserByEmail(String email);
}
