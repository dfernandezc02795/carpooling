package com.project.cardpooling.service.impl;

import com.project.cardpooling.dto.UserDto;
import com.project.cardpooling.mapper.UserMapper;
import com.project.cardpooling.model.User;
import com.project.cardpooling.repository.IUserRepository;
import com.project.cardpooling.service.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class UserServiceImpl implements IUserService {

    IUserRepository userRepository;
    UserMapper userMapper;

    @Autowired
    public UserServiceImpl(IUserRepository userRepository,UserMapper userMapper){
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDto saveUser(UserDto user) {
        User userEntiy = userMapper.getEntityFromDto(user);
        userRepository.save(userEntiy);
        return userMapper.getDtoFromEntity(userEntiy);
    }

    @Override
    public List<UserDto> findUserByEmail(String email) {
        List<User> userEntiy = userRepository.findUserByEmail(email);
        return userMapper.getListDto(userEntiy);
    }
}
