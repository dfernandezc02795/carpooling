package com.project.cardpooling.repository;

import com.project.cardpooling.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository extends JpaRepository <User, Long> {
    @Query(value = "select * from users e where e.email like %:email%",nativeQuery=true)
    List<User> findUserByEmail(@Param("email") String email);
}
