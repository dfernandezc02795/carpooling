package com.project.cardpooling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CardpoolingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardpoolingApplication.class, args);
	}

}
