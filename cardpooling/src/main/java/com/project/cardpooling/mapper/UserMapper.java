package com.project.cardpooling.mapper;

import com.project.cardpooling.dto.UserDto;
import com.project.cardpooling.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mappings({
            @Mapping(target = "id",source = "id"),
            @Mapping(target = "name",source = "name"),
            @Mapping(target = "lastname",source = "lastname"),
            @Mapping(target = "telephone",source = "telephone"),
            @Mapping(target = "address",source = "address"),
            @Mapping(target = "email",source = "email"),
            @Mapping(target = "password",source = "password")
    })
    UserDto getDtoFromEntity(User entity);
    User getEntityFromDto(UserDto dtoUser);

    List<UserDto> getListDto(List<User> users);
}
