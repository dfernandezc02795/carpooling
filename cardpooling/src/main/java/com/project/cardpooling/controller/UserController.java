package com.project.cardpooling.controller;

import com.project.cardpooling.dto.UserDto;
import com.project.cardpooling.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import java.util.List;

@RestController
public class UserController {

    IUserService userService;

    @Autowired
    public UserController(IUserService userService){this.userService = userService; }

    @PostMapping(path = "/createUser")
    public ResponseEntity<UserDto> saveUser(@Valid  @RequestBody UserDto user){
        try{
            UserDto userSave = userService.saveUser(user);
            return new ResponseEntity<UserDto>(userSave, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<UserDto>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "/find/{email}")
    public List<UserDto> findUserByEmail(@Email  @PathVariable(name = "email") String email){
        try{
            return  userService.findUserByEmail(email);
        }catch (Exception e){
            return null;
        }
    }
}
